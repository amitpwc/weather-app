import React, { useState } from "react";
import PropTypes from 'prop-types';
import './index.scss';

const Search = (props) => {

    const { placeholder, searchHandler } = props;
    const [searchText, setSearch] = useState('');

    const inputSearchHandler = (event) => {
        event.preventDefault();
        setSearch(event.target.value);
    }

    const submitHandler = () => {
        searchHandler(searchText);
        setSearch('');
    }

    return (
        <div>
            <input className="input-search" type="search" placeholder={placeholder} value={searchText} onChange={inputSearchHandler} />
            <button className="search-btn" disabled={searchText === ''} onClick={submitHandler}>
                Search
            </button>
        </div>
    )
}

Search.propTypes = {
    placeholder: PropTypes.string.isRequired,
    searchHandler: PropTypes.func.isRequired
};

export default Search;