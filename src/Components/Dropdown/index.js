import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './index.scss';

const Dropdown = (props) => {

    const { label, optionsData, selectedUnitOptios } = props;
    const [options, setOptions] = useState('Imperial');

    const selectHandler = (event) => {
        event.preventDefault();
        setOptions(event.target.value);
        selectedUnitOptios(event.target.value);
    }

    return (
        <div className="drop-down">
            <label className="label-style">{label}</label>
            <select className="select-options" value={options} onChange={(event) => selectHandler(event)}>
                {optionsData && optionsData.map((val) => {
                    return (
                        <option value={val.value} key={val.id}>{val.value}</option>
                    )
                })}
            </select>
        </div>
    );
}

Dropdown.propTypes = {
    label: PropTypes.string.isRequired,
    selectedUnitOptios: PropTypes.func.isRequired,
    optionsData: PropTypes.array.isRequired
};

export default Dropdown;