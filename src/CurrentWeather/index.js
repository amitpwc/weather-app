import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import './index.scss';

const CurrentWeather = (props) => {

    const [today, setToday] = useState(null);
    const [option, setOptions] = useState('Imperial');

    useEffect(() => {
        setToday(props.today);

    }, [props.today]);

    useEffect(() => {
        setOptions(props.option);
    }, [props.option]);

    const dateConvertIntoDay = (date) => {
        return new Date(date).toLocaleString('en-us', { weekday: 'long' });
    }

    const nth = function (d) {
        if (d > 3 && d < 21) return 'th';
        switch (d % 10) {
            case 1: return "st";
            case 2: return "nd";
            case 3: return "rd";
            default: return "th";
        }
    }

    return (
        <div>
            {today && today.country && (
                <div>
                    <div className="today-forecast">
                        <p className="country-name">{today && today.country}</p>
                        <p className="date">
                            {today && dateConvertIntoDay(today.local_time) + ', ' + (new Date(today.local_time)).toLocaleString('default', { month: 'long' }) + ' ' + (new Date(today.local_time)).getDate()}
                            <sup>{today && nth((new Date(today.local_time)).getDate())}</sup>
                        </p>
                        <p className="date">Overcast</p>
                        <div>
                            <span>
                                <img src={today && today.icon_url} alt="no_image" />
                            </span>
                            <span className="temperature">
                                &nbsp;&nbsp;&nbsp;<span className="number">{option === 'Metric' ? today.temp_c : today.temp_f}</span>
                                <span className="cf">{option === 'Metric' ? <span>&#8451;</span> : <span>&#x2109;</span>} </span>
                            </span>
                        </div>
                    </div>
                    <div className="middle">
                        <p>Humidity: {today.humidity}%</p>
                        <p>Wind: {option === 'Metric' ? today.wind_kph + ' kph ' + today.wind_direction : today.wind_mph + ' mph ' + today.wind_direction}</p>
                    </div>
                </div>
            )}
        </div>
    );
}

CurrentWeather.propTypes = {
    today: PropTypes.object,
    option: PropTypes.string.isRequired
};

export default CurrentWeather;