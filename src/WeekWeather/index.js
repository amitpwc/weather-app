import React from "react";
import PropTypes from 'prop-types';
import './index.scss';

const WeekWeather = (props) => {

    const { data, option } = props;

    return (
        <div className="main">
            {data && data.length > 0 ?
                <div className="parent">
                    {data && data.map((val) => {
                        return (
                            <div key={val.day}>
                                <div className="max">{val.day}</div>
                                <div>
                                    <img src={val.icon_url} alt="no_image" />
                                </div>
                                <div>
                                    <span className="max">{option === 'Metric' ? val.max_temp_c : val.max_temp_f}</span><sup>&#8304;</sup>&nbsp;&nbsp;
                                    <span className="min">{option === 'Metric' ? val.min_temp_c : val.min_temp_f}</span><sup>&#8304;</sup>
                                </div>
                            </div>
                        )
                    })
                    }
                </div>
                :
                <div>
                    <h1>No Data Found!</h1>
                </div>
            }
        </div>
    )
}

WeekWeather.propTypes = {
    data: PropTypes.array.isRequired,
    option: PropTypes.string.isRequired
};

export default WeekWeather;