import React, { useState, useEffect } from "react";
import CurrentWeather from "../CurrentWeather";
import WeekWeather from "../WeekWeather";
import Search from "../Components/Search";
import Dropdown from "../Components/Dropdown";
import Axios from 'axios';
import './index.scss';

const Weather = () => {

  const [week, setWeek] = useState([]);
  const [today, setToday] = useState(null);
  const [loading, setLoading] = useState(true);
  const [option, setOption] = useState('Imperial');
  const optionsData = [
    { id: 1, value: 'Imperial' },
    { id: 2, value: 'Metric' }
  ];

  useEffect(() => {
    getData();
  },
    // eslint-disable-next-line
    []);

  // Get the Current location name
  const getData = async () => {
    const res = await Axios.get('https://geolocation-db.com/json/');
    getCurrentNowWeather(res.data.city);
    getWeekendData(res.data.city);
  };

  // Calling weeksData API based on the IP address(location/city)
  const getWeekendData = (location_name) => {

    const headers = {
      'Content-Type': 'application/json',
      Authorization: process.env.REACT_APP_API_KEY,
    };

    const data = {
      days: 7,
      location: location_name,
    };

    Axios.post(`${process.env.REACT_APP_BASE_API_URL}weather/Forecast`, data, { headers })
      .then((res) => {
        let data = res.data.forecast;
        data && data.map((val, index) => {
          if (index === 0) {
            val.day = 'Today';
          }
          else {
            val.day = dateConvertIntoDay(val.date);
          }
          return val;
        });
        setLoading(false);
        setWeek(data);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  }

  // Calling API based on search location name
  const getCurrentNowWeather = (location_name) => {

    const headers = {
      'Content-Type': 'application/json',
      Authorization: process.env.REACT_APP_API_KEY,
    };

    const now = {
      location: location_name
    }

    Axios.post(`${process.env.REACT_APP_BASE_API_URL}weather/Now`, now, { headers })
      .then((res) => {
        setToday(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  const dateConvertIntoDay = (date) => {
    return new Date(date).toLocaleString('en-us', { weekday: 'long' });
  }

  const searchHandler = (location_name) => {
    getCurrentNowWeather(location_name);
  }

  const selectedUnitOptios = (selected_options) => {
    setOption(selected_options);
  }

  return (
    <div>
      {!loading ?
        <div>
          <div className="search-unit">
            <Search searchHandler={searchHandler} placeholder={'Enter a country name, city'} />
            <Dropdown optionsData={optionsData} label={'Select Units type : '} selectedUnitOptios={selectedUnitOptios} />
          </div>
          <CurrentWeather today={today} option={option} />
          <WeekWeather data={week} option={option} />
        </div>
        :
        <div className="loading-center">
          <h3>Loading...</h3>
        </div>
      }
    </div>
  );
}

export default Weather;