import React from 'react';
import Weather from '../Weather';
import './index.scss';

const Home = () => {

  return (
    <div className="main-div">
      <Weather />
    </div>
  );
};

export default Home;
