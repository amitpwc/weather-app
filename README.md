# Weather React JS Project

## What is the use of this Repo

This Project is a Simple Weather ReactJS Project which demonstrates the following
1. Creating a Component in React
2. Making HTTP calls
3. Communicating between parent and child component
4. Re-usable Component
5. Using Hooks and functional component

## Weather App screenshot
![app-screenshot](/uploads/2f37e167ff00343c3f4a673595309baa/app-screenshot.PNG)

## Prerequisites

### Install Node JS
Refer to https://nodejs.org/en/ to install nodejs

### Install create-react-app
Install create-react-app npm package globally. This will help to easily run the project and also build the source files easily. Use the following command to install create-react-app

```bash
npm install -g create-react-app
```

## Cloning and Running the Application in local

Clone the project into local

Install all the npm packages. Go into the project folder and type the following command to install all npm packages

```bash
npm install
```

In order to run the application Type the following command

```bash
npm start
```

The Application Runs on **localhost:3000**

## Application design

#### Components

1. **Home** Component : This Component is a Home page and importing the weatherHome component.

2. **WeatherHome** Component: This component is the main weather component, where all sub-component are connected here.

3. **CurrentWeather** Component: This component is showing the current weather, based on the search location name and updating the Humidity and wind values.

4. **WeekWeather** Component: This component is showing the 7 day current location basis weather data and When you search the location basis weather, this values is not changed, only currentWeather values is changed.

5. **SearchComponent** Component: This component is re-usable component. Just import the component and send the placehodler message and function name.

6. **DropdownComponent** Component: This component is a re-usable component. Just import the componet and send the dropdown values, labelname and function name.


#### HTTP client

**axios** library is used to make HTTP Calls


## Resources

**create-react-app** : The following link has all the commands that can be used with create-react-app
https://github.com/facebook/create-react-app

**ReactJS** : Refer to https://reactjs.org/ to understand the concepts of ReactJS
